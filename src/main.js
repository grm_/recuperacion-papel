import Vue from 'vue'
import App from './App.vue'
import store from './store'
import CreditCard from 'creditcard.js'
import Vuelidate from 'vuelidate'
import VMasker from 'vanilla-masker'

Vue.prototype.$CreditCard = CreditCard

// Vue.use(CreditCard)
Vue.use(Vuelidate)
Vue.use(VMasker)


Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
